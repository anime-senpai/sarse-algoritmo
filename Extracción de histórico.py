#!/usr/bin/env python
# coding: utf-8

# In[ ]:


def lecturaReclamos(fechaIni, fechaFin):
    sqlEngine = create_engine('mysql+pymysql://senpai:Kumpa305@127.0.0.1:3306/osimac')
    dbConnection = sqlEngine.connect()
    reclamos = pd.read_sql("select ID_ATNCN,ID_MOTV,NECSDD,FCHA_CRCION from mac_atncion where (FCHA_CRCION BETWEEN "+fechaIni+" AND "+fechaFin+")", dbConnection);
    return reclamos


# In[ ]:


#Limpiar tags html
def cleanHtmlTags(data):
    p = re.compile(r'<.*?>')
    p1 = p.sub('', data)
    return html.unescape(p1)


# In[ ]:


def procesar_1_reclamo(reclamo,nosimb,stopwords_esp):
    
    #eliminar tags html
    reclamo_sin_html = cleanHtmlTags(reclamo)
    
    #eliminar puntuacion
    reclamo_no_puntuacion = re.findall(nosimb,(re.sub(r'[(\\\\)]n', ' ', reclamo_sin_html)))
    #print(reclamo_no_puntuacion)

    #todo a minusculas
    reclamo_minuscula = " ".join(str(x).lower() for x in reclamo_no_puntuacion)
    #print(reclamo_minuscula)

    #eliminar stopwords
    reclamo_no_stopwords = " ".join(token for token in reclamo_minuscula.split() if token not in stopwords_esp)
    #print(reclamo_no_stopwords)

    #lemma y stemma
    stemmer = SnowballStemmer('spanish')
    reclamo_stemmed = " ".join(stemmer.stem(token) for token in reclamo_no_stopwords.split())
    return reclamo_stemmed


# In[ ]:


def procesarReclamos(reclamos):
    #puntuacion !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    #puntuacion
    nosimb = r"\b[\w\!\$\%\'\,\-\.\?]+\b"
    
    #stopwords
    nltk.download('stopwords')
    stopwords_esp = nltk.corpus.stopwords.words('spanish')
    #print(stopwords_esp)
    
    #titles_alphanum=[]
    reclamos_procesados = []
    for i in range (len(reclamos)):
        reclamo_procesado = procesar_1_reclamo(reclamos[i],nosimb,stopwords_esp)
        
        reclamos_procesados.append(reclamo_procesado)
        
    return reclamos_procesados


# In[ ]:


def guardarReclamos(reclamos_dict):
    tableName = "reclamo_hist"
    dataFrame = pd.DataFrame(data=reclamos_dict)
    sqlEngine = create_engine('mysql+pymysql://senpai:Kumpa305@127.0.0.1:3306/sarse')
    dbConnection = sqlEngine.connect()
    try:
        frame = dataFrame.to_sql(tableName, dbConnection, if_exists='append');

    except ValueError as vx:
        print(vx)
        
    except Exception as ex:   
        print(ex) 

    finally:
        dbConnection.close()


# In[ ]:


from sqlalchemy import create_engine
import pymysql
import pandas as pd
from nltk.stem import SnowballStemmer
import nltk
import html
import re
import numpy as np
#from datetime import datetime

extraerHistorico(fechaIni, fechaFin)


# In[ ]:


def extraerHistorico(fechaIni, fechaFin):
    reclamos_a_procesar = []

    reclamos = lecturaReclamos(fechaIni, fechaFin)
    reclamos_texto = reclamos["NECSDD"].to_list()
    #reclamos_respuesta = reclamos["RESPT"].to_list()
    reclamos_id = reclamos["ID_ATNCN"].to_list()
    reclamos_motivo = reclamos["ID_MOTV"].to_list()
    reclamos_fechas = reclamos["FCHA_CRCION"].to_list()

    reclamos_procesados = procesarReclamos(reclamos_texto)

    reclamos_dict = {}
    reclamos_dict["idreclamo_hist"] =  reclamos_id
    reclamos_dict["id_motivo"] =  reclamos_motivo
    reclamos_dict["texto"] =  reclamos_procesados
    reclamos_dict["fecha_registro"] =  reclamos_fechas

    guardarReclamos(reclamos_dict)

