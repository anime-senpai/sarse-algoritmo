from sqlalchemy import create_engine
import pymysql
import pandas as pd
import pickle
from nltk.stem import SnowballStemmer
import nltk
import html
import re
import numpy as np
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from flask import Flask, jsonify
from flask import request
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

def eliminarReclamos(fechaIni, fechaFin):
    dbConnection = pymysql.connect(host="sarsetesis.c3uvdaazqqu8.us-east-1.rds.amazonaws.com", user="senpai", password="Kumpa305", db="sarse", autocommit=True)
    
    cursorInstance = dbConnection.cursor()
    sqlDeleteRows   = "Delete from reclamo_hist where (fecha_registro BETWEEN '"+fechaIni+"' AND '"+fechaFin+"')"
    cursorInstance.execute(sqlDeleteRows)

def lecturaReclamos(fechaIni, fechaFin):
    sqlEngine = create_engine('mysql+pymysql://senpai:Kumpa305@sarsetesis.c3uvdaazqqu8.us-east-1.rds.amazonaws.com:3306/osimac')
    dbConnection = sqlEngine.connect()
    reclamos = pd.read_sql("select ID_ATNCN,ID_MOTV,NECSDD,FCHA_MDFCCION from mac_atncion where (FCHA_MDFCCION BETWEEN '"+fechaIni+"' AND '"+fechaFin+"')", dbConnection);
    return reclamos


def procesar_1_reclamo(reclamo,nosimb,stopwords_esp):
    
    #eliminar tags html
    reclamo_sin_html = cleanHtmlTags(reclamo)
    
    #eliminar puntuacion
    reclamo_no_puntuacion = re.findall(nosimb,(re.sub(r'[(\\\\)]n', ' ', reclamo_sin_html)))
    #print(reclamo_no_puntuacion)

    #todo a minusculas
    reclamo_minuscula = " ".join(str(x).lower() for x in reclamo_no_puntuacion)
    #print(reclamo_minuscula)

    #eliminar stopwords
    reclamo_no_stopwords = " ".join(token for token in reclamo_minuscula.split() if token not in stopwords_esp)
    #print(reclamo_no_stopwords)

    #lemma y stemma
    stemmer = SnowballStemmer('spanish')
    reclamo_stemmed = " ".join(stemmer.stem(token) for token in reclamo_no_stopwords.split())
    return reclamo_stemmed


def procesarReclamos(reclamos):
    #puntuacion !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
    #puntuacion
    nosimb = r"\b[\w\!\$\%\'\,\-\.\?]+\b"
    
    #stopwords
    nltk.download('stopwords')
    stopwords_esp = nltk.corpus.stopwords.words('spanish')
    #print(stopwords_esp)
    
    #titles_alphanum=[]
    reclamos_procesados = []
    for i in range (len(reclamos)):
        reclamo_procesado = procesar_1_reclamo(reclamos[i],nosimb,stopwords_esp)
        
        reclamos_procesados.append(reclamo_procesado)
        
    return reclamos_procesados
    

#Limpiar tags html
def cleanHtmlTags(data):
    p = re.compile(r'<.*?>')
    p1 = p.sub('', data)
    return html.unescape(p1)

def procesar_reclamo(reclamo):

    nltk.download('stopwords')
    stopwords_esp = nltk.corpus.stopwords.words('spanish')
    
    nosimb = r"\b[\w\!\$\%\'\,\-\.\?]+\b"
    
    #eliminar tags html
    reclamo_sin_html = cleanHtmlTags(reclamo)
    
    #eliminar puntuacion
    reclamo_no_puntuacion = re.findall(nosimb,(re.sub(r'[(\\\\)]n', ' ', reclamo_sin_html)))
    #print(reclamo_no_puntuacion)

    #todo a minusculas
    reclamo_minuscula = " ".join(str(x).lower() for x in reclamo_no_puntuacion)
    #print(reclamo_minuscula)

    #eliminar stopwords
    reclamo_no_stopwords = " ".join(token for token in reclamo_minuscula.split() if token not in stopwords_esp)
    #print(reclamo_no_stopwords)

    #lemma y stemma
    stemmer = SnowballStemmer('spanish')
    reclamo_stemmed = " ".join(stemmer.stem(token) for token in reclamo_no_stopwords.split())
    return reclamo_stemmed



def guardarReclamos(reclamos_dict):
    tableName = "reclamo_hist"
    dataFrame = pd.DataFrame(data=reclamos_dict)
    sqlEngine = create_engine('mysql+pymysql://senpai:Kumpa305@sarsetesis.c3uvdaazqqu8.us-east-1.rds.amazonaws.com:3306/sarse')
    dbConnection = sqlEngine.connect()
    try:
        frame = dataFrame.to_sql(tableName, dbConnection, if_exists='append',index=False);

    except ValueError as vx:
        print(vx)
        
    except Exception as ex:   
        print(ex) 

    finally:
        dbConnection.close()





def extraerHistorico(fechaIni, fechaFin):

    eliminarReclamos(fechaIni, fechaFin)

    reclamos = lecturaReclamos(fechaIni, fechaFin)
    reclamos_texto = reclamos["NECSDD"].to_list()
    #reclamos_respuesta = reclamos["RESPT"].to_list()
    reclamos_id = reclamos["ID_ATNCN"].to_list()
    reclamos_motivo = reclamos["ID_MOTV"].to_list()
    reclamos_fechas = reclamos["FCHA_MDFCCION"].to_list()

    reclamos_procesados = procesarReclamos(reclamos_texto)

    reclamos_dict = {}
    reclamos_dict["idreclamo_hist"] =  reclamos_id
    reclamos_dict["id_motivo"] =  reclamos_motivo
    reclamos_dict["texto"] =  reclamos_procesados
    reclamos_dict["fecha_registro"] =  reclamos_fechas
    
    guardarReclamos(reclamos_dict)
    
    return len(reclamos_procesados)


def scrapping(dni):
    driver = webdriver.Chrome(executable_path="/usr/lib/chromium-browser/chromedriver")

    try:
        driver.get("https://eldni.com/buscar-por-dni")
        input_dni = driver.find_element_by_xpath('//*[@id="dni"]')
        
        input_dni.send_keys(dni)

        buscar = driver.find_element_by_xpath('/html/body/div/div/div/div[1]/div[1]/form/div/div/span/button/i')

        buscar.click()

        dni_scrap = driver.find_element_by_xpath('/html/body/div/div/div/div[2]/div/div/table/tbody/tr/th')
        dni_text = dni_scrap.text

        nombres_scrap = driver.find_element_by_xpath('/html/body/div/div/div/div[2]/div/div/table/tbody/tr/td[1]')
        nombres_text = nombres_scrap.text

        apellido_pat_scrap = driver.find_element_by_xpath('/html/body/div/div/div/div[2]/div/div/table/tbody/tr/td[2]')
        apellido_pat_text = apellido_pat_scrap.text

        apellido_mat_scrap = driver.find_element_by_xpath('/html/body/div/div/div/div[2]/div/div/table/tbody/tr/td[3]')
        apellido_mat_text = apellido_mat_scrap.text
        driver.close()
        return dni_text, nombres_text, apellido_pat_text, apellido_mat_text
    except:
        driver.close()


@app.route('/clasificar', methods=['POST'])
def clasificar_reclamo():
    data = request.get_json()
    print(data['texto'])
    
    reclamo_procesado = procesar_reclamo(data['texto'])
    
    #carga el vectorizador tf-idf
    word_vectorizer_filename = 'word_vectorizer2.pickle'
    word_vectorizer = pickle.load(open(word_vectorizer_filename, 'rb'))

    #carga elmodelo entrenado
    filename = 'SGDClassifier.pickle'
    model = pickle.load(open(filename, 'rb'))
    
    motivo = 1001
    
    reclamo_list_predict = [reclamo_procesado]
    tfidf_matrix_predict = word_vectorizer.transform(reclamo_list_predict)
    
    Xnew  = tfidf_matrix_predict
    Ynew = model.predict(Xnew)
    motivo = Ynew[0]
    item = motivo.item()
    #motivo = modelo.predict(reclamo_procesado)
    
    return jsonify({"motivo": item})



@app.route('/extraer', methods=['POST'])
def extraccion_reclamo():
    data = request.get_json()
    fechaIni = data['fechaIni']
    fechaFin = data['fechaFin']
    print(fechaFin)
    cant = extraerHistorico(fechaIni, fechaFin)
    
    return jsonify({"cantidad": cant})
    

@app.route('/dni/<dni>', methods=['GET'])
def obtener_dni(dni):
    print(dni)
    #try:
    dni_text, nombres_text, apellido_pat_text, apellido_mat_text = scrapping(dni)
    return jsonify({"dni": dni_text, "nombres": nombres_text, "apellido_paterno": apellido_pat_text, "apellido_materno": apellido_mat_text})
    
    #except:
    #    dni_text = '0'
    #    return jsonify({"dni": dni_text})
    


@app.route('/', methods=['POST'])
def hello():
    data = request.get_json()
    return jsonify({"about": "Hello World!"})

if __name__ == '__main__':
    app.run(host='0.0.0.0')

    
  